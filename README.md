# Prerequisites

Preferred environment build would be using nuxt and pug

Sass, Vue and Tailwind versions are a must

```json
{
  "dependencies": {
    "nuxt": "^2.15.8",
    "pug": "^3.0.0",
    "pug-plain-loader": "^1.0.0",
    "sass": "^1.53.0",
    "sass-loader": "^10.3.0",
    "vue": "^2.6.14",
    "vue-server-renderer": "^2.6.14",
    "vue-template-compiler": "^2.6.14",
    "webpack": "^4.46.0"
  },
  "devDependencies": {
    "@nuxt/postcss8": "^1.1.3",
    "@nuxtjs/tailwindcss": "^4.2.1",
    "autoprefixer": "^10.4.7",
    "postcss": "^8.4.14",
    "tailwindcss": "^3.0.24"
  }
}
```

**Node 16.19 is a must**

For Docker

```bash
FROM node:16.19-alpine3.16
```

For working locally

```bash
nvm install 16.19
```

```bash
nvm use 16.19
```

# IDE

```bash
indent_style = space
indent_size = 4
```

# Vue standards

The project and templates will be component driven so site sections are to be broken down into seperate components this will make it easier for us to build the page editor where the client can edit the site

Keeping the below structure in vue templates will help us to integrate code faster

1. v-if, v-for etc
2. @ event emits
3. prop binds
4. everything else

```pug
<template lang="pug">
    TemplateContainer(
        v-if="page && event_products"
        @view-basket="viewBasket"
        @view-product="viewProduct"
        :basket="$store.state.event.type === 'store' ? true : false"
        :items="$store.state.basket.products.length"
        :page="{ page: page }"
        :products="event_products"
        class="col-5"
    )
</template>
```

```html
<custom-columns
    v-if="modalTitle === 'Add or remove columns'"
    @custom-table-all="customTableAll"
    @custom-table-update="customTableUpdate"
    :tableColumns="customTableCols"
    :tableHeader="tableHeader || []"
    class="col-6"
/>
```

Pages/parent components must be the data driver and pass data down via props to the child

```js
<script>
    props: {
        page: {
            default: () => ({}),
            type: Object
        }
    }
</script>
```

Keeping to the below vue script section in this order will again help us to integrate faster

**Note:** If using Nuxt, we don't need to import the components

```js
<script>
    // ============================================================================================
    import { mixin } from "@/mixins/mixin.js";

    /**
     * @author Jason Curren
     * @description
     * ============================================================================================
     */
    export default {
        mixins: [mixin],
        name: "ComponentName",
        props: {
            someProp: {
                default: "",
                type: String
            }
        },
        data() {
            return {
                someData: {}
            }
        },
        created() {
            //
        },
        mounted() {
            //
        },
        computed: {
            //
        },
        filters: {
            //
        },
        methods: {
            //
        },
        watch: {
            //
        }
    }
</script>
```

# Tailwind standards

To avoid messy unreadable CSS, we will be using Sass and Tailwind with CSS properties and TW utlities being in alphabetical order

If a bunch of utilities are being used to button, typography, forms etc over and over, just create a reuable class instead of repeating the same thing over and over

```pug
<template lang="pug">
    p(
        v-if="message"
        :class="`border-${type} text-${type}`"
        class="border mb-4 p-4 rounded-2xl"
    ) {{ message }}
</template>

<script>
    export default {
        name: "MessageInfo",
        props: {
            message: {
                default: "",
                type: String
            },
            type: {
                default: "error",
                type: String
            }
        }
    }
</script>
```

```scss
.source-tree {
    position: relative;

    $position: 20%;
    $thick: 2px;

    &:before {
        background-color: var(--color-border);
        content: "";
        height: $thick;
        left: -$position;
        position: absolute;
        top: calc(50% - 1px);
        width: $position;
    }

    &:after {
        background-color: var(--color-border);
        content: "";
        height: 50%;
        left: -$position;
        position: absolute;
        top: 0;
        width: $thick;
    }
}
```

For media queries, run these after mobile css/utilities

```pug
<template lang="pug">
    nav(class="bg-light border-t border-border w-full lg:border-none lg:static lg:w-1/4")
</template>
```

```scss
$h1-size: 2.71rem !default;

h1 {
    font-size: $h1-size;

    @media (min-width: theme("screens.lg")) {
        font-size: $h1-size * 2;
    }
}
```

# Project setup

The templates will be content and theme editable by the client, we will need the below to be implmented so we can make is adjustable

Sass imported file

```scss
:root {
    --color-border: #e4e4e4;
    --color-error: #df3737;
    --color-fade: #00000080;
    --color-light: #ffffff;
    --color-primary: #416FEC;
    --color-secondary: #0D162F;
    --color-third: #0D162F;
    --color-success: #4ec58a;
}
```

Tailwind import file

```js
module.exports = {
    theme: {
        colors: {
            black: "#000",
            white: "#fff",
            border: "var(--color-border)",
            error: "var(--color-error)",
            fade: "var(--color-fade)",
            light: "var(--color-light)",
            primary: "var(--color-primary)",
            secondary: "var(--color-secondary)",
            success: "var(--color-success)",
            third: "var(--color-third)",
        }
    }
}
```